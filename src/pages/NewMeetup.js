import { useNavigate } from "react-router-dom";

import NewMeetupFrom from "../components/meetups/NewMeetupFrom";

function NewMeetupPage() {
  const navigate = useNavigate();

  function handleAddMeetup(meetupData) {
    fetch(
      "https://react-getting-started-6a005-default-rtdb.firebaseio.com/meetups.json",
      {
        method: "POST",
        body: JSON.stringify(meetupData),
        header: {
          "Content-Type": "application/json"
        }
      }
    ).then(() => {
      // replace: true stops you from using the back button.
      navigate("/", {replace: true});
    });
  }

  return (
    <section>
      <h1>Add New Meetup</h1>
      <NewMeetupFrom onAddMeetup={handleAddMeetup} />
    </section>
  );
}

export default NewMeetupPage;
